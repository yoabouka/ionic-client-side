import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import ValidationError from '../../../shared/validation.error';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;
  errors: [];
  private messageError: ValidationError = new ValidationError();
  private message = '';
  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.registerForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onSignUpUser() {
    const email = this.registerForm.get('email').value;
    const password = this.registerForm.get('password').value;
    const firstName = this.registerForm.get('first_name').value;
    const lastName = this.registerForm.get('last_name').value;
    const username = this.registerForm.get('username').value;
    console.log(`registerig in: ${username}`);
    this.authService.registerUser(email, password, username, firstName, lastName, 'ROLE_USER').subscribe(
        () => {
          this.router.navigate(['/tabs', 'tabs', 'tab1']);
        },
        (error => {
          if (error instanceof ValidationError) {
            this.messageError = error;
          } else {
            this.message = error;
          }
        })
    );
  }
}
