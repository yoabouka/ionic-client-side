import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import ValidationError from '../../../shared/validation.error';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  message = '';
  messageError: ValidationError = new ValidationError();
  LoginForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.LoginForm = this.formBuilder.group(
        {
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required, Validators.minLength(6)]]
        }
    );
  }

  onSignInUSer() {
    const email = this.LoginForm.get('email').value;
    const password = this.LoginForm.get('password').value;
    console.log(`logging in: ${email}`);
    this.authService.authenticate(email, password).subscribe(
        () => {
          this.router.navigate(['/tabs', 'tabs', 'tab1']);
        },
        (error => {
            if (error instanceof ValidationError) {
                this.messageError = error;
            } else {
                this.message = error;
            }
        })
    );
  }


}
