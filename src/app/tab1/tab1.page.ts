import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(private  httpClient: HttpClient) {
    this.loadProducts();
  }

  loadProducts(): void {
    const url = `${environment.api_url}/products/products-list`;
    console.log('url', url);
    this.httpClient.get(url).subscribe(products => console.log('articles', products));
  }
}
