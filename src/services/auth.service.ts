import {Injectable} from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import {environment} from '../environments/environment';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {catchError, map, mergeMap} from 'rxjs/operators';
import ValidationError from '../shared/validation.error';


export const LOGIN_API_ROUTE = environment.api_url + '/users/login-client';
export const REGISTER_API_ROUTE = environment.api_url + '/users/register';
const INFO_API = environment.api_url + '/users/auth/info';
const LOGOUT_API = environment.api_url + '/users/logout';
const REFRESH_API_ROUTE = environment.api_url + '/users/refresh';

class LoginResponse {
    accessToken: string;
    refreshToken: string;
}

class RegisterResponse {
    accessToken: string;
    refreshToken: string;
}

class RefreshResponse {
    accessToken: string;
}

class UserInfo {
    email: string;
    username: string;
    firstName: string;
    lastName: string;
    roles: [];
}

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    private jwt: JwtHelperService = new JwtHelperService();
    private authStatus: BehaviorSubject<boolean> = new BehaviorSubject(this.isAuthenticated());

    constructor(private  httpClient: HttpClient) {
    }

    private errorHandler(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error(`authentication error': ${error.error.message}`);
        } else {
            console.error(error);
            console.error(`bad auth response: ${error.status}: ${error.statusText} ${JSON.stringify(error.error)}`);
            const validationError = new ValidationError();
            validationError.records(error.error);
            return throwError(validationError);
        }
        return throwError('authentication attempt failed');
    }

    // subscribe to get authentication status updates
    subscribe(next: (status: boolean) => void) {
        this.authStatus.subscribe(next);
    }

    // Log user in and get refresh/access tokens
    authenticate(email: string, password: string) {
        return this.httpClient.post<LoginResponse>(LOGIN_API_ROUTE, { email, password})
            .pipe(
                mergeMap(response => {
                    // store JWTs
                    localStorage.setItem('accessToken', response.accessToken);
                    localStorage.setItem('refreshToken', response.refreshToken);

                    // now get user info
                    const opts = {
                        headers: new HttpHeaders({
                            // tslint:disable-next-line:max-line-length
                            Authorization: 'Bearer ' + localStorage.getItem('accessToken')  // tslint:disable-line:object-literal-key-quotes
                        })
                    };
                    return this.httpClient.get<UserInfo>(INFO_API, opts).pipe(
                        map(userInfo => {
                            localStorage.setItem('email', userInfo.email);
                            localStorage.setItem('username', String(userInfo.username));
                            localStorage.setItem('firstName', String(userInfo.firstName));
                            localStorage.setItem('lastName', String(userInfo.lastName));
                            localStorage.setItem('roles', String(userInfo.roles));
                            this.authStatus.next(true);
                        })
                    );
                }),
                catchError(this.errorHandler)
            );
    }
    registerUser(email: string, password: string, username: string, firstName: string, lastName: string, roles: string) {
        return this.httpClient.post<RegisterResponse>(REGISTER_API_ROUTE, { email, password, username, firstName, lastName, roles })
            .pipe(
                mergeMap(response => {
                    // store JWTs
                    localStorage.setItem('accessToken', response.accessToken);
                    localStorage.setItem('refreshToken', response.refreshToken);

                    // now get user info
                    const opts = {
                        headers: new HttpHeaders({
                            // tslint:disable-next-line:max-line-length
                            Authorization: 'Bearer ' + localStorage.getItem('accessToken')  // tslint:disable-line:object-literal-key-quotes
                        })
                    };
                    return this.httpClient.get<UserInfo>(INFO_API, opts).pipe(
                        map(userInfo => {
                            localStorage.setItem('email', userInfo.email);
                            localStorage.setItem('username', String(userInfo.username));
                            localStorage.setItem('firstName', String(userInfo.firstName));
                            localStorage.setItem('lastName', String(userInfo.lastName));
                            localStorage.setItem('roles', String(userInfo.roles));
                            this.authStatus.next(true);
                        })
                    );
                }),
                catchError(this.errorHandler)
            );
    }

    // Log user out, clear stored tokens
    deauthenticate() {
        const opts = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + localStorage.getItem('refreshToken')  // tslint:disable-line:object-literal-key-quotes
            })
        };
        localStorage.clear();
        this.authStatus.next(false);
        return this.httpClient.post(LOGOUT_API, {}, opts)
            .pipe(
                map(response => null),
                catchError(this.errorHandler)
            );
    }


    // Get access token, automatically refresh if necessary
    getAccessToken(): Observable<string> {
        const accessToken = localStorage.getItem('accessToken');
        const refreshToken = localStorage.getItem('refreshToken');
        if (!this.jwt.isTokenExpired(accessToken)) {
            return new BehaviorSubject(accessToken);
        } else if (!this.jwt.isTokenExpired(refreshToken)) {
            console.log('refreshing access token');
            const opts = {
                headers: new HttpHeaders({
                    Authorization: 'Bearer ' + refreshToken
                })
            };
            return this.httpClient.post<RefreshResponse>(REFRESH_API_ROUTE, {}, opts).pipe(
                map(response => {
                    localStorage.setItem('accessToken', response.accessToken);
                    console.log('authentication refresh successful');
                    return response.accessToken;
                })
            );
        } else {
            return throwError('refresh token is expired');
        }
    }

    // User is logged in
    isAuthenticated(): boolean {
        return localStorage.getItem('email') !== null &&
            !this.jwt.isTokenExpired(localStorage.getItem('refreshToken'));
    }

    // User is an client
    isUser(): boolean {
       const  roles = localStorage.getItem('roles');
       let role = 'ROLE_USER';
       for (const r of roles) {
           role = r;
        }
       return role === 'ROLE_USER';
    }

    // get email
    getEmail(): string {
        return localStorage.getItem('email');
    }

}
